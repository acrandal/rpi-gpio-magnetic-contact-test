#!/usr/bin/env python
#
# Magnetic door switch test code
# Uses hardware:
#   Raspberry PI (model 2),
#   Normal door/security magnetic contact switch
#
# Designed to just test GPIO code for this switch
# (1) Waits on user to be ready
# (2) Waits on edge detection (both) of GPIO 18
# (3) Tests and prints current magenet state
#       Magnet present - "Door Closed"
#       Magnet away    - "Door Open"
#
#   This work is licensed under a Creative Commons 
#   Attribution-NonCommercial-ShareAlike 4.0 
#   International License.
#
#   Copyright 2016
#   Aaron Crandall
#   acrandal@gmail.com
#

import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

print("Program tests a single magnetic contact switch on a GPIO pin.")
print("Attach contact switch between pin 2 (3.3v) and BCM GPIO 18.")
print("Then move magnet into range and back of the contact sensor.")
print("Enter Ctrl-C (^C) to terminate.")
print("")
raw_input("Press Enter when ready\n>")

try:  
    while(True):
        print " [x] Waiting for either edge on port 18"  
        GPIO.wait_for_edge(18, GPIO.BOTH)  
        if GPIO.input(18):
            print(" [!] Door Closed")
        else:
            print(" [!] Door open")
 
except KeyboardInterrupt:  
    GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
    print("Done.")
GPIO.cleanup()           # clean up GPIO on normal exit  
print("Done.")
